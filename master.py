import micropython; micropython.alloc_emergency_exception_buf(100)
import pyb  # pyboard specific libraries

import ecss_can_minimal as can
from master_od import MasterNodeObjectDictionary
from slave_od import SlaveNodeObjectDictionary, SLAVE_NODE_ID


# settings for the prototype test scenario, all times in ms
PERIOD_SCET = 10000
PERIOD_TC = 100
PERIOD_TM_REQ = 5
SLAVE_NODE_HB_MESSAGE = COB_ID_HEARTBEAT + SLAVE_NODE_ID
SLAVE_NODE_TM_REQ_REPLY_MESSAGE = COB_ID_TPDO_3 + SLAVE_NODE_ID

# create network of main and redundant bus
network = can.Network()
bus_a = can.Bus(1)
bus_b = can.Bus(2)
network.connect(bus_a, bus_b)
network.set_active_bus(bus_a)

# create master node from object dictionary
master_od = MasterNodeObjectDictionary()
master_node = can.Node(master_od)
network.add_node(master_node)

# create slave node from object dictionary
slave_od = SlaveNodeObjectDictionary()
slave_node = Node(slave_od)
network.add_node(slave_node)

# configure master node
master_node.state = STATE_CODE_INITIALIZING
master_node.heartbeat_producer = HeartbeatProducer(master_node)
master_node.sync = SyncProducer(master_node)

# define message reception filters
master_node.network.set_filters([
    {"can_id": SLAVE_NODE_HB_MESSAGE, "can_mask": 0x7FF},
    {"can_id": SLAVE_NODE_TM_REQ_REPLY_MESSAGE, "can_mask": 0x7FF},
    ])

# set state to operational and start heartbeat and sync service
master_node.state = STATE_CODE_OPERATIONAL
master_node.heartbeat_producer.start()
master_node.sync.start()

# following loop is in a try statement to allow for exit via Ctrl-C
try:
    print("info: -----starting test scenario-----")
    start_time = pyb.millis()
    last_time_SLAVE_HB = 0
    last_time_SCET = 0
    last_time_TC = 0
    last_time_TM_REQ = 0
    slave_node_alive = True
    scet_sent = 0
    tc_sent = 0
    tm_req_sent = 0
    tm_received = 0

    while True:
        current_time = pyb.millis()

        # need to flush received messages on the inactive bus
        if network.get_inactive_bus().message_available():
            _ = network.get_inactive_bus().receive()

        # poll for received messages on the active bus
        if network.active_bus.message_available():
            message = network.active_bus.receive()

            # check if slave node heartbeat received
            if message.can_id == SLAVE_NODE_HB_MESSAGE:
                last_time_SLAVE_HB = current_time
                pyb.LED(LED_YELLOW).toggle()
                if slave_node_alive is False:
                    print("info: receiving slave node heartbeat")
                    slave_node_alive = True

            # check if slave node TM REQ reply received
            elif message.can_id == SLAVE_NODE_TM_REQ_REPLY_MESSAGE:
                tm_received += 1

        # check if slave node heartbeat timeout
        if slave_node_alive is True:
            if current_time - last_time_SLAVE_HB >= \
                    master_node.od[OD_INDEX_CONSUMER_HEARTBEAT_TIME].value:
                print("warning: no slave node heartbeat")
                slave_node_alive = False

        # send SCET
        if current_time - last_time_SCET >= PERIOD_SCET:
            last_time_SCET = current_time
            master_od.scet_coarse.value = current_time // 1000
            fraction_of_second = current_time % 1000
            master_od.scet_fine.value = (fraction_of_second * 256) // 1000
            print("info: send SCET {} sec + {}/256 sec".format(
                master_od.scet_coarse.value, master_od.scet_fine.value))
            if master_node.pdo[TPDO_SPACECRAFT_SCET_GET].transmit(master_node.id):
                scet_sent += 1

        # send dummy TC
        if current_time - last_time_TC >= PERIOD_TC:
            last_time_TC = current_time
            master_od.tc_code.value = 0x2211  # a dummy TC code
            master_od.tc_param.value = 0x665544332211  # a dummy TC param
            if master_node.pdo[TPDO_SEND_TC].transmit(SLAVE_NODE_ID):
                tc_sent += 1

        # send dummy TM REQ
        if current_time - last_time_TM_REQ >= PERIOD_TM_REQ:
            last_time_TM_REQ = current_time
            master_od.tm_request.value = 0x2211  # a dummy TM REQ code value
            if master_node.pdo[TPDO_SEND_TM_REQ].transmit(SLAVE_NODE_ID):
                tm_req_sent += 1

except KeyboardInterrupt:
    master_node.heartbeat_producer.stop()
    master_node.sync.stop()
    network.shutdown()
    print("info: -----run test scenario for {} seconds-----".format(
        (pyb.millis() - start_time) / 1000))
    print("info: SCET sent: {}".format(scet_sent))
    print("info: TC sent: {}".format(tc_sent))
    print("info: TM REQ sent: {}".format(tm_req_sent))
    print("info: TM REQ reply received: {}".format(tm_received))
