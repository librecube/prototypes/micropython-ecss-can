

class Network:
    """Redundant CAN bus network containing one or more nodes."""
    def __init__(self):
        self.nodes = {}
        self.active_bus = None
        self.bus_a = None
        self.bus_b = None

    def connect(self, bus_a=None, bus_b=None):
        if bus_a is None and bus_b is None:
            raise Exception("'bus_a' or 'bus_b' must be specified.")
        self.bus_a = bus_a
        self.bus_b = bus_b

    def disconnect(self):
        self.bus_a = None
        self.bus_b = None
        self.active_bus = None

    def shutdown(self):
        if self.bus_a:
            self.bus_a.shutdown()
        if self.bus_b:
            self.bus_b.shutdown()

    def set_active_bus(self, active_bus):
        if active_bus is not self.bus_a and active_bus is not self.bus_b:
            raise Exception("The selected bus is not valid.")
        self.active_bus = active_bus

    def get_active_bus(self):
        return self.active_bus

    def get_inactive_bus(self):
        if self.active_bus == self.bus_a:
            return self.bus_b
        elif self.active_bus == self.bus_b:
            return self.bus_a
        else:
            return None

    def switch_bus(self):
        """Switch from the current active bus to the other one."""
        if self.active_bus == self.bus_a:
            self.set_active_bus(self.bus_b)
        elif self.active_bus == self.bus_b:
            self.set_active_bus(self.bus_a)
        else:
            raise Exception("No bus is active.")

    def send_message(self, message):
        if self.active_bus is None:
            raise Exception("No bus is active.")
        self.active_bus.send(message)

    def add_node(self, node):
        self.nodes[node.id] = node
        node.network = self
        return node

    def __getitem__(self, node_id):
        return self.nodes[node_id]

    def __delitem__(self, node_id):
        del self.nodes[node_id]

    def __iter__(self):
        return iter(self.nodes)

    def __len__(self):
        return len(self.nodes)

    def set_filters(self, can_filters=None):
        """Apply filtering to messages of the busses of the network."""
        self.bus_a.set_filters(can_filters)
        self.bus_b.set_filters(can_filters)
