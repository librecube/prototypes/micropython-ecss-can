

class ObjectDictionary:
    """Representation of the object dictionary as a Python dictionary.
    The object dictionary is a table with 16 bit index and 8 bit subindex
    and contains all the configuration data and process variables. If for
    a certain index location no subindex entries are used, then only a single
    Variable is placed there. Otherwise, a Record of Variables is placed there
    with a length equal to the number of subindeces used."""
    def __init__(self, node_id=None):
        self.indices = {}
        self.node_id = node_id

    def __getitem__(self, index):
        return self.indices.get(index)

    def __iter__(self):
        return iter(sorted(self.indices))

    def __len__(self):
        return len(self.indices)

    def __contains__(self, index):
        return index in self.indices

    def add_object(self, index, obj):
        """Add an object at a given index of the object dictionary."""
        self.indices[index] = obj


class Variable:
    """Simple variable."""
    def __init__(self, value, name=None):
        self.value = value
        self.name = name

    def __getitem__(self, subindex):
        if subindex != 0:
            raise Exception("A {} only has subindex 0".format(Variable))
        return self


class Record:
    def __init__(self, name=None):
        self.name = name
        self.subindices = {}

    def __getitem__(self, subindex):
        return self.subindices.get(subindex)

    def __len__(self):
        return len(self.subindices)

    def __iter__(self):
        return iter(sorted(self.subindices))

    def __contains__(self, subindex):
        return subindex in self.subindices

    def add_member(self, subindex, var):
        if type(var) is not Variable:
            raise Exception("Must provide an instance of {}".format(Variable))
        self.subindices[subindex] = var
