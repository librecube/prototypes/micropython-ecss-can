from .pdo import Pdo


class Node:
    """A node (master or slave). It is part of a network, has an id, is
    configured through an object dictionary, and has 4 Transmit and 4 Receive
    PDOs and PDO mappings."""
    def __init__(self, object_dictionary):
        self.network = None
        self.od = object_dictionary
        self.id = object_dictionary.node_id
        self.state = None
        self.heartbeat_producer = None
        self.heartbeat_consumer = None
        self.pdo = {1: Pdo(self,
                           0x1800, 0x1A00, 0x1400, 0x1600),
                    2: Pdo(self,
                           0x1801, 0x1A01, 0x1401, 0x1601),
                    3: Pdo(self,
                           0x1802, 0x1A02, 0x1402, 0x1602),
                    4: Pdo(self,
                           0x1803, 0x1A03, 0x1403, 0x1603)}
