import pyb

from .message import Message


class Bus:
    """A bus is a physical CAN connection (channel) consisting of a CAN High
    and CAN Low line."""

    MAX_CAN_FILTERBANKS = 14
    DEFAULT_TIMEOUT = 5000  # timeout in milliseconds

    def __init__(self, channel=None):
        """Initializes the CAN bus. Baud rate is fixed to 1 Mbps.
        The settings for other baud rates would be as follows:
        125 kbps: prescaler=16, sjw=1, bs1=14, bs2=6
        250 kbps: prescaler=8, sjw=1, bs1=14, bs2=6
        500 kbps: prescaler=4, sjw=1, bs1=14, bs2=6
        """
        if channel not in [1, 2]:
            raise Exception("Channel must be 1 or 2.")
        self.channel = channel
        self.fifo = channel - 1  # 0 or 1
        self.filterbank = channel - 1  # 0 or 1
        self.can = pyb.CAN(self.channel, mode=pyb.CAN.NORMAL,
                           prescaler=2, sjw=1, bs1=14, bs2=6)
        # allocate the maximum amount of filter banks to each CAN controller
        self.can.initfilterbanks(Bus.MAX_CAN_FILTERBANKS)
        # set callback function to indicate receive buffer congestion
        self.can.rxcallback(self.fifo, self.rxcallback)

    def rxcallback(self, bus, reason):
        """This function is called whenever a CAN message is received and
        it checks whether the message buffer is getting too full."""
        if reason == 0:
            pass
        if reason == 1:
            print('warning: can bus fifo full')
        if reason == 2:
            print('error: can bus fifo overflow')

    def send(self, message, timeout=None):
        """Transmit a message to CAN bus. Timeout is in seconds."""
        try:
            if timeout is None:
                self.can.send(message.data, message.can_id)
            else:
                timeout_ms = int(1000 * timeout)
                self.can.send(message.data, message.can_id, timeout=timeout_ms)
        except Exception:
            print("error: message transmit failed.")

    def message_available(self):
        """Returns True if one or more messages in the buffer."""
        return self.can.any(self.fifo)

    def receive(self, timeout=None):
        """Block waiting for a message from the Bus."""
        if timeout is not None:
            timeout = int(1000 * timeout)
        else:
            timeout = Bus.DEFAULT_TIMEOUT

        can_id, _, _, data = self.can.recv(self.fifo, timeout=timeout)
        return Message(can_id, data)

    def set_filters(self, can_filters=None):
        """Apply filtering to all messages received by this Bus.
        Calling without passing any filters will reset the applied filters.

        :param list can_filters:
            A list of dictionaries each containing a "can_id" and a "can_mask".

            >>> [{"can_id": 0x11, "can_mask": 0x21}]

            A filter matches,
            when <received_can_id> & can_mask == can_id & can_mask
        """
        if can_filters is None:
            self.can.initfilterbanks(Bus.MAX_CAN_FILTERBANKS)
            return False

        if not isinstance(can_filters, list):
            raise Exception("Filters must be a list of dictionaries.")

        if len(can_filters) > Bus.MAX_CAN_FILTERBANKS * 2:
            raise Exception("A maximum of {} filters can be defined.".format(
                Bus.MAX_CAN_FILTERBANKS * 2))

        bank = 0  # start from bank 0
        params = []  # a list that will hold two id and filter values
        for can_filter in can_filters:
            if not isinstance(can_filter, dict):
                raise Exception("Filters must be a list of dictionaries.")
            if 'can_id' not in can_filter:
                raise Exception("'can_id' is not provided.")
            if 'can_mask' not in can_filter:
                raise Exception("'can_mask' is not provided.")

            params.append(can_filter['can_id'])
            params.append(can_filter['can_mask'])
            if len(params) == 4:
                self.can.setfilter(bank, pyb.CAN.MASK16, self.fifo, params)
                params = []
                bank += 1
        if len(params) > 0:  # complete a halfly filled params list
            params.append(0x000)
            params.append(0x7FF)
            self.can.setfilter(bank, pyb.CAN.MASK16, self.fifo, params)

    def shutdown(self):
        self.can.deinit()
