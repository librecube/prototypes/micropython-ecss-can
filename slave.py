import micropython  # micropython language specific libraries
import pyb  # pyboard specific libraries

from ecss_can_minimal.bus import Bus
from ecss_can_minimal.network import Network
from ecss_can_minimal.node import Node
from ecss_can_minimal.heartbeat import HeartbeatProducer, HeartbeatConsumer
from ecss_can_minimal.sync import SyncConsumer
from ecss_can_minimal.constants import *
from master_od import MasterNodeObjectDictionary
from slave_od import SlaveNodeObjectDictionary, SLAVE_NODE_ID


micropython.alloc_emergency_exception_buf(100)  # needed for debug messages


# settings for the prototype test scenario, all times in ms
MASTER_NODE_HB_MESSAGE = COB_ID_HEARTBEAT + MASTER_NODE_ID
MASTER_NODE_SCET_MESSAGE = COB_ID_TPDO_1 + MASTER_NODE_ID
MASTER_NODE_TC_MESSAGE = COB_ID_RPDO_2 + SLAVE_NODE_ID
MASTER_NODE_TM_REQ_MESSAGE = COB_ID_RPDO_3 + SLAVE_NODE_ID

# create network of main and redundant bus
network = Network()
bus_a = Bus(1)
bus_b = Bus(2)
network.connect(bus_a, bus_b)
network.set_active_bus(bus_a)

# create master node from object dictionary
master_od = MasterNodeObjectDictionary()
master_node = Node(master_od)
network.add_node(master_node)

# create slave node from object dictionary
slave_od = SlaveNodeObjectDictionary(SLAVE_NODE_ID)
slave_node = Node(slave_od)
network.add_node(slave_node)

# configure slave node
slave_node.state = STATE_CODE_INITIALIZING
slave_node.heartbeat_producer = HeartbeatProducer(slave_node)
slave_node.heartbeat_consumer = HeartbeatConsumer(slave_node)
slave_node.sync = SyncConsumer(slave_node)

# define message reception filters
slave_node.network.set_filters([
    {"can_id": COB_ID_SYNC, "can_mask": 0x7FF},
    {"can_id": MASTER_NODE_HB_MESSAGE, "can_mask": 0x7FF},
    {"can_id": MASTER_NODE_SCET_MESSAGE, "can_mask": 0x7FF},
    {"can_id": MASTER_NODE_TC_MESSAGE, "can_mask": 0x7FF},
    {"can_id": MASTER_NODE_TM_REQ_MESSAGE, "can_mask": 0x7FF},
    ])

# set state to operational and start heartbeat and sync service
slave_node.state = STATE_CODE_OPERATIONAL
slave_node.heartbeat_producer.start()
slave_node.heartbeat_consumer.start()
slave_node.sync.start()

# following loop is in a try statement to allow for exit via Ctrl-C
try:
    print("info: -----starting test scenario-----")
    start_time = pyb.millis()
    scet_received = 0
    tc_received = 0
    tm_req_received = 0
    tm_sent = 0

    while True:
        current_time = pyb.millis()

        # need to flush received messages on the inactive bus
        if network.get_inactive_bus().message_available():
            _ = network.get_inactive_bus().receive()

        # poll for received messages on the active bus
        if network.active_bus.message_available():
            message = network.active_bus.receive()

            # check if SYNC received
            if message.can_id == COB_ID_SYNC:
                slave_node.sync.received()

            # check if master node heartbeat received
            elif message.can_id == MASTER_NODE_HB_MESSAGE:
                slave_node.heartbeat_consumer.received()

            # check if SCET received
            elif message.can_id == MASTER_NODE_SCET_MESSAGE:
                SCET = int.from_bytes(message.data[3:8], 'little')
                SCET_ms = int.from_bytes(message.data[2:3], 'little')
                print("info: received SCET {} sec + {}/256 sec".format(
                    SCET, SCET_ms))
                scet_received += 1

            # check if TC received
            elif message.can_id == MASTER_NODE_TC_MESSAGE:
                tc_code = int.from_bytes(message.data[0:2], 'little')
                tc_param = int.from_bytes(message.data[2:], 'little')
                # execute function here according to tc_code and tc_param
                # e.g. execute_telecommand(tc_code, tc_param)
                tc_received += 1

            # check if TM REQ received
            elif message.can_id == MASTER_NODE_TM_REQ_MESSAGE:
                tm_req_code = int.from_bytes(message.data[0:2], 'little')
                slave_od.tm_req_code.value = tm_req_code
                # adjust PDO mapping to map to the index, subindex of request
                slave_od.tm_mapping.value = "{:04x}{:02x}".format(
                    OD_INDEX_TM + (tm_req_code >> 8), tm_req_code & 0xFF) + "30"
                # send out TM reply
                tm_req_received += 1
                if slave_node.pdo[3].transmit(slave_node.id):
                    tm_sent += 1

except KeyboardInterrupt:
    slave_node.heartbeat_producer.stop()
    slave_node.heartbeat_consumer.stop()
    slave_node.sync.stop()
    network.shutdown()
    print("info: -----run test scenario for {} seconds-----".format(
        (pyb.millis() - start_time) / 1000))
    print("info: SCET received: {}".format(scet_received))
    print("info: TC received: {}".format(tc_received))
    print("info: TM REQ received: {}".format(tm_req_received))
    print("info: TM sent: {}".format(tm_sent))
