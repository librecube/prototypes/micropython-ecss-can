ECSS-CAN Bus Prototype in Micropython
=====================================

This is a prototype implementation of the `ECSS <http://ecss.nl/>`_ standard
*ECSS-E-ST-50-15C – CANbus extension protocol*, available
`here <http://ecss.nl/standard/ecss-e-st-50-15c-space-engineering-canbus-extension-protocol-1-may-2015/>`_.

The ECSS standard makes use of the `CAN Bus <https://en.wikipedia.org/wiki/CAN_bus>`_
as physical and datalink layer and a modified `CANopen <https://en.wikipedia.org/wiki/CANopen>`_
protocol. The ECSS CAN Bus is to be used as spacecraft command and control bus.

This implementation is done in the `MicroPython <http://micropython.org>`_ language,
a subset of Python 3, ported to a handful of 32-bit microcontrollers.

This implementation shall serve as boilerplate code to form the robust and
reliable command and control bus for a LibreCube platform elements, consisting
of a network of master and slave nodes.

Detailed information on the setup and usage can be found in the
`wiki <https://gitlab.com/librecube/prototypes/ecss-can-bus-micropython/wikis>`_.
